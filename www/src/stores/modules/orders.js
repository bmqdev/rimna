import menuService from '../../services/menu';

const state = {
  menu: [],
};

const mutations = {
  SET_MENU(state, mane) {
    state.menu = mane;
  },
};

const actions = {
  setBAN: ({ commit }, ban) => commit('SET_BAN', ban),
  loadMenu: ({ commit }, payload) => {
    return menuService.getState(payload).then((data) => {
      // console.log(data)
      if (data != null) {
        commit('SAVE_REG_PROFILE', data);
      }
    });
  },
};

const getters = {
  getMenu: (state) => state.menu,
};

export default {
  state,
  getters,
  mutations,
  actions,
};
