import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Read from '../views/examples/connect-api/Read.vue'

import Example from '../views/examples/Example.vue'
import Example1 from '../views/examples/Example1.vue'
import Example2 from '../views/examples/Example2.vue'
import Example3 from '../views/examples/Example3-Concept.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  { path: '/user/:id', component: User },
  { path: '/read', component: Read },
  { path: '/exmaple', component: Example },
  { path: '/exmaple1', component: Example1 },
  { path: '/exmaple2', component: Example2 },
  { path: '/exmaple3', component: Example3 },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
