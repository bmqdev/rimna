
import axios from 'axios'
import config from '../config'

const loadMenu = (payload) => {
  console.log('payload', payload);
  return axios({
    url: `${config.ENDPOINTS.RIMNA_SERVICE}/api/menu`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    }
  })
}

export default {
  loadMenu
}
