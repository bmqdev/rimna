# www

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Go ot src/config, Copy file "config.example.js" and rename to "config.js", Edit params below 
```
{
  line: {
    port: "9000",  // default 9000 in this project
    channelAccessToken: "YOUR_LINE_BOT_TOEKN",
    channelSecret: "YOUR_LINE_BOT_CHANNEL_SECRET"
  },
  google: {
    key: 'YOU_GOOGLE_API_KEY'
  }
}

```