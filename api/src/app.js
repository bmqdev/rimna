import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import path from 'path'
import { middleware } from '@line/bot-sdk'

import { router as algorithm } from './routers/algorithm'
import { router as line } from './routers/line'
import { router as google } from './routers/google'
import { router as test } from './routers/test'

import config from './config/config'

const PORT = 9000
const init = async () => {
  try {
    const app = express()
    app.use(cors())
    app.use('/public', express.static(path.join(__dirname, '/../public')));
    app.use('/algorithm', algorithm)
    // app.use('/api/line/webhook', middleware(config.line))
    // app.use('/api/line', line)
    // app.use('/api/google', google)
    // app.use('/api/test', test)

    app.use(bodyParser.json())
    app.use(([body, status], req, res, next) => {
      res.status(status).json(body)
      next()
    })
    app.listen(PORT);
  } catch (e) {
    console.log(e)
  }
}

init()
