const conf = {
  line: {
    port: "",
    channelAccessToken: "",
    channelSecret: ""
  },
  google: {
    key: ''
  }
}

export default conf