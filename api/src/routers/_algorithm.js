import express from 'express';
import redis from 'redis';
import _ from 'lodash';

import {resp} from '../utils/resp';
import {controller} from '../controllers/controllers';
import '../data/menu';
import {menu, relationExtra, remainings, workStation} from '../data/menu';
import {items, orderList, waitingList} from '../data/orders';
import {datetime} from '../utils/time';

let day = '2020-10-17';
let timenow = `12:00:00`;
let format = 'HH:mm:ss';

let orders = [];
let waitings = [];
let stations = [];
let chefs = [];

async function calculate() {
  stations = await getStationList();
  chefs = await getChefList();
  orders = [];
  waitings = [];

  const orderItems = _.chain(items)
    .groupBy('order_id')
    .map((value, key) => ({order_id: key, data: value}))
    .value();
  

  let res = [];  
  for (var i = 0; i < orderItems.length; i++) {
    let item = orderItems[i].data;
    // order work station
    item = await groupingStation(item);

    // split
    // const gs = await grouping(item.data);
    // console.log('gs', gs);
  }

  // await getOrderList();
  // await getWaitingList();
  // console.log('orders', JSON.stringify(orderItems));

  // let wlist = [];


  // const groups = await grouping(items);
  // const tasks = await addExtraTask(groups);

  // const tasks2 = await reorder(tasks);
  // console.log('tasks', JSON.stringify(tasks));
  // // const queue = await queuing(tasks2.filter(t => t.is_extra == 1));
  // const queue = await queuing(tasks2);
  // await queuingWaitings();

  // console.log('orders', orders);
  // console.log('tasks2', JSON.stringify(waitings));

  // _.orderBy(orders, ['start', 'asc']).map((atm) => {
  //   // let child = (atm.child || []).reduce((total, b) => {
  //   //   return total + b.qty;
  //   // }, 0);
  //   let oc = (atm.child || []).map(m => m.order_id);
  //   let order = `${atm.order_id || (atm.ref_order || {}).order_id }| ${oc.join('|')}`
  //   console.log(`q ${atm.queue}, order: ${order}, chef: ${atm.chef_id}, ${atm.start}, ${atm.end}, ${atm.menu_name} ${atm.total_qty > 1 ? `* ${atm.total_qty}` : ''}, qty ${atm.total_qty}`);

  //   (atm.make_with || []).map((mk) => {
  //     let oc = (atm.child || []).map(m => m.order_id);
  //     let order = `${mk.order_id}| ${oc.join(',')}`
  //     console.log(`q ${mk.queue}, order: ${order}, chef: ${mk.chef_id}, ${mk.start}, ${mk.end}, ${mk.menu_name} ${mk.total_qty > 1 ? `* ${mk.total_qty}` : ''}, qty ${mk.total_qty} , <------ ${atm.queue}`);
  //   });

  //   // // atm.qty = atm.child.length + 1;
  //   // // return atm
  // });

  // console.log('waitings---------------->');

  // _.orderBy(waitings, ['start', 'desc']).map((w) => {
  //   // let child = (w.child || []).reduce((total, b) => {
  //   //   return total + b.qty;
  //   // }, 0);
  //   let oc = (w.child || []).map(m => m.order_id);
  //   let order = `${w.order_id || (w.ref_order || {}).order_id }| ${oc.join('|')}`
  //   console.log(`q ${w.queue}, order: ${order}, chef: ${w.chef_id}, ${w.start}, ${w.end}, ${w.menu_name} ${w.total_qty > 1 ? `* ${w.total_qty}` : ''}, qty ${w.total_qty}`);
  //   // // atm.qty = atm.child.length + 1;
  //   // // return atm
  // });

  // console.log(JSON.stringify(orders));
  // console.log(JSON.stringify(waitings));
}

async function groupingStation(items) {
  for (let i = 0; i < items.length; i++) {
    let m = items[i];
    console.log('m', m);
  }
  return items;
}

async function grouping(items) {
  // split - items

  let splited = [];
  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    let info = menu.find((m) => m.id == item.menu_id) || {};
    let station = workStation.find((s) => s.id == info.work_station_id) || {};

    for (let sp = 0; sp < item.qty; sp++) {
      splited.push({
        ...item,
        qty: 1,
        work_station_id: station.id,
      });
    }
  }

  // console.log('splited', JSON.stringify(splited));

  let res = [];
  let queue = 1;
  for (let o = 0; o < splited.length; o++) {
    let item = splited[o];
    // console.log('item', item);
    let info = menu.find((m) => m.id == item.menu_id) || {};
    let station = workStation.find((s) => s.id == info.work_station_id);

    // merge with orders
    let cont = false;
    for (let d = 0; d < waitings.length; d++) {
      if (waitings[d].menu_id == item.menu_id) {
        if (waitings[d].total_qty + 1 <= waitings[d].max) {
          waitings[d].child.push(item);
          // waitings[d].total_qty += 1;
          // waitings[d].extra_task.total_qty += 1;
          // waitings[d].extra_task.ref_order.total_qty += 1;
          // waitings[d].extra_task.ref_order.child.push(item);
          // console.log('waitings', JSON.stringify(waitings));
          // console.log('waitings', waitings[d].menu_name, item.menu_name, waitings[d].menu_id, item.menu_id);
          cont = true;
        }
      }
    }
    if (cont == true) {
      continue;
    }

    let nexts = splited.slice(o + 1);
    if (item.queue != undefined || item.menu_id == undefined) {
      continue;
    }

    let temp = {
      ...item,
      total_qty: item.qty,
      queue,
      child: [],
    };

    for (let j = 0; j < nexts.length; j++) {
      if (nexts[j].menu_id == item.menu_id) {
        // // add group menu
        // orders.splice(i+1, 0, nexts[j])
        // check capacity
        // console.log('item', item.meunu_name, item.eo);
        // (item.child || []).length
        let childQty = 0;
        if ((temp.child || []).length > 0) {
          childQty = temp.child.reduce(function (total, b) {
            return total + b.qty;
          }, 0);
        }

        // console.log('childQty', childQty);
        let currectQty = item.qty + childQty;
        if (currectQty < info.max) {
          temp.total_qty = currectQty + 1;
          temp.child.push(nexts[j]);

          // remove group menu
          let key = j + o + 1;
          splited.splice(key, 1, {});
        }
      }
    }
    res.push(temp);
    queue++;
  }

  // for (let w = 0; w < waitings.length; w++) {
  // }
  // console.log('res', JSON.stringify(waitings));
  return res;
}

async function reorder(items) {
  // console.log('items', JSON.stringify(items));
  let res = [];
  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    let extra = relationExtra.find((r) => r.main_id == item.menu_id);
    let extraInfo = await getInfo({menu_id: (extra || {}).extra_id});
    let task = undefined;
    if (extra != undefined) {
      let info = extraInfo.info;
      if (info.auto_reorder == 1) {
        items[i].used_extra_menu_id = extra.extra_id;
        // console.log('items', items);
        // remaining
        let remain = remainings.find((r) => r.id == extra.extra_id);
        // console.log('remain', remain);
        let inprogress = items.filter((re) => re.menu_id == extra.extra_id) || [];
        let inpQty = inprogress.reduce((total, item) => {
          return total + item.total_qty;
        }, 0);

        let used = items.filter((re) => re.is_extra != 1 && re.used_extra_menu_id == extra.extra_id) || [];
        let usedQty = used.reduce((total, item) => {
          return total + item.total_qty || 0;
        }, 0);

        console.log('-------->');
        let qty = remain.qty + inpQty - usedQty;
        console.log('qty', qty, remain.qty, inpQty, ' used:', usedQty);
        console.log('remain', info.reorder_point);

        if (qty <= info.reorder_point) {
          items.unshift({
            ...info,
            menu_id: info.id,
            menu_name: info.name,
            qty: info.default_qty,
            total_qty: info.default_qty,
            time: item.time,
            queue: 0,
          });
          ++i;
        }
        //
        // console.log('-->', item.menu_name, info.name);
      }
    }
  }
  // console.log('res', JSON.stringify(items));
  return items;
}

async function addExtraTask(items) {
  let res = [];
  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    let extra = relationExtra.find((r) => r.main_id == item.menu_id);
    let extraInfo = await getInfo({menu_id: (extra || {}).extra_id});
    let task = undefined;
    if (extra != undefined) {
      if (extraInfo.info.auto_reorder == 0) {
        task = {
          ...extraInfo.info,
          id: undefined,
          menu_id: extraInfo.info.id,
          menu_name: extraInfo.info.name,
          time: item.time,
          queue: item.queue,
          qty: item.total_qty,
          total_qty: item.total_qty,
          ref_order: {
            ...item,
            // ...pick,
            // extra_task: undefined,
          },
        };
        res.push(task);
      }
    }
    let itemInfo = await getInfo({menu_id: item.menu_id});
    res.push({
      ...item,
      ...itemInfo.info,
    });
  }
  return res;
}

async function queuing(inputs) {
  let res = [];
  let items = inputs;
  while (items.length > 0) {
    let item = items[0];
    if (item.is_extra == 1) {
      // from reorder point
      let extraTask = item;
      let station = await getAvailableStation(extraTask);
      let nextChef = await getNextChef();
      // console.log('station------>', item.menu_name, station.availble);
      if (station.availble == true) {
        // statino ว่าง
        let extraStart = datetime.moment(extraTask.time).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        let extraEnd = datetime.moment(extraStart).add(extraTask.cook_time, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        orders.push({
          ...extraTask,
          start: extraStart,
          end: extraEnd,
          chef_id: nextChef.chef,
          used_station: station,
        });
      } else {
        // station ไม่ว่าง
        let extraStart = datetime.moment(station.availble_time).add(1, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        let extraEnd = datetime.moment(extraStart).add(extraTask.cook_time, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        waitings.push({
          ...extraTask,
          waiting: 1,
          start: extraStart,
          end: extraEnd,
          // chef_id: nextChef.chef,
          used_station: station,
        });
      }
    } else if (item.extra_task != undefined) {
      // let extraTask = item.extra_task;
      // let station = await getAvailableStation(item.extra_task);
      // let nextChef = await getNextChef();
      // if (station.availble == true) {
      //   // statino ว่าง
      //   let extraStart = datetime.moment(extraTask.time).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   let extraEnd = datetime.moment(extraStart).add(extraTask.cook_time, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   orders.push({
      //     ...extraTask,
      //     start: extraStart,
      //     end: extraEnd,
      //     chef_id: nextChef.chef,
      //     used_station: station,
      //   });
      //   let plus = item.cook_time + item.child.length * item.plus_time;
      //   let wStart = datetime.moment(extraEnd).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   let wEnd = datetime.moment(wStart).add(plus, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   waitings.push({
      //     ...item,
      //     waiting: 1,
      //     start: wStart,
      //     end: wEnd,
      //     // chef_id: nextChef.chef,
      //   });
      // } else {
      //   // station ไม่ว่าง
      //   let extraStart = datetime.moment(station.availble_time).add(1, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   let extraEnd = datetime.moment(extraStart).add(extraTask.cook_time, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   waitings.push({
      //     ...extraTask,
      //     waiting: 1,
      //     start: extraStart,
      //     end: extraEnd,
      //     // chef_id: nextChef.chef,
      //     used_station: station,
      //   });
      //   let plus = item.cook_time + item.child.length * item.plus_time;
      //   let wStart = datetime.moment(extraEnd).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   let wEnd = datetime.moment(wStart).add(plus, 'minutes').format('YYYY-MM-DD HH:mm:ss');
      //   waitings.push({
      //     ...item,
      //     waiting: 1,
      //     start: wStart,
      //     end: wEnd,
      //     // chef_id: nextChef.chef,
      //   });
      // }
    } else {
      //
      let mEx = await makeWithExtra();
      let nextChef = await getNextChef();

      // check remain
      let waitForExtra = await waitRemainExtra(item);
      if ((waitForExtra || {}).waiting == true) {
        // console.log('waitForExtra', waitForExtra);
        let wStart = datetime.moment(waitForExtra.available_time).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        let wEnd = datetime
          .moment(wStart)
          .add(item.cook_time * (item.total_qty || 0), 'minutes')
          .format('YYYY-MM-DD HH:mm:ss');

        waitings.push({
          ...item,
          waiting: 1,
          start: wStart,
          end: wEnd,
          // chef_id: nextChef.chef,
        });
        items.shift();
        continue;
      }

      let canMakeWithExtra = false;
      if (mEx != undefined) {
        if (mEx.min_end == 0) {
          // console.log('mEx', mEx);
          canMakeWithExtra = true;
        } else if (mEx.min_end < nextChef.available_time) {
          // console.log('mEx.min_end', mEx.min_end, nextChef.available_time);
          canMakeWithExtra = true;
        }
      } else {
        canMakeWithExtra = false;
      }
      if (canMakeWithExtra == true) {
        // ทำพร้อมกับ extra task ได้
        // console.log('mEx', mEx);

        let initStart = mEx.start;
        // let initStart = item.time;
        // initStart = (initStart > mEx.start) ? initStart : mEx.start ;

        let start = datetime.moment(initStart).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        let plusMatchingTime = 2;
        let end = datetime
          .moment(start)
          .add(item.cook_time + plusMatchingTime, 'minutes')
          .format('YYYY-MM-DD HH:mm:ss');
        // orders.push();
        let key = mEx.order_index;
        if (orders[key].make_with == undefined) {
          orders[key].make_with = [];
        }
        orders[key].make_with.push({
          ...item,
          start: start,
          end: end,
          chef_id: mEx.chef_id,
          plus_matching_time: plusMatchingTime,
        });
      } else {
        // ทำพร้อมกับ extra task ไม่ได้ หา chef ที่ว่าง
        let initStart = item.time;
        if (nextChef.available_time != null) {
          initStart = nextChef.available_time;
          // initStart = (initStart > nextChef.available_time) ? initStart :nextChef.available_time;
        }

        let start = datetime.moment(initStart).add(0, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        let end = datetime.moment(start).add(item.cook_time, 'minutes').format('YYYY-MM-DD HH:mm:ss');

        orders.push({
          ...item,
          start: start,
          end: end,
          chef_id: nextChef.chef,
        });
      }
    }
    // check station
    items.shift();
  }
  return inputs;
}

async function makeWithExtra() {
  // let extraList = _.orderBy(orders.filter(o => o.is_extra == 1), ['end'], ['asc']);
  for (var i = 0; i < orders.length; i++) {
    let item = orders[i];
    if (item.is_extra == 1) {
      if ((item.make_with || []).length == 0) {
        return {
          ...item,
          order_index: i,
          min_end: 0,
          min_end_limit: item.end,
        };
      } else {
        // can make with this and not over time
        let last = _.last(item.make_with);
        // console.log('last', last);
        return {
          ...item,
          order_index: i,
          start: last.end,
          min_end: last.end,
          min_end_limit: item.end,
        };
      }
    }
  }

  return undefined;
}

async function getAvailableStation(item) {
  let info = stations.find((s) => s.id == item.work_station_id);
  for (var i = 0; i < info.stations.length; i++) {
    let item = info.stations[i];
    // item.tasks = (orders).concat(waitings).filter((o) => (o.used_station || {}).sub_station_id == item.station && o.work_station_id == item.station);
    item.tasks = orders.concat(waitings).filter((o) => (o.used_station || {}).work_station_id == info.id && (o.used_station || {}).sub_station_id == item.station);
    // ว่าง
    // console.log('item', '', item.station , JSON.stringify(item.tasks.length));
    if (item.tasks.length == 0) {
      return {
        ...info,
        work_station_id: info.id,
        sub_station_id: item.station,
        availble: true,
        stations: undefined,
      };
    }
  }

  let res = {};
  for (var j = 0; j < info.stations.length; j++) {
    let item = info.stations[j];
    let tasks = orders.concat(waitings).filter((o) => (o.used_station || {}).work_station_id == info.id && (o.used_station || {}).sub_station_id == item.station);
    let latest = _.last(tasks) || {};
    if (res.availble_time == undefined) {
      res = {
        ...info,
        work_station_id: info.id,
        sub_station_id: item.station,
        availble: false,
        availble_time: latest.end,
        stations: undefined,
      };
      continue;
    }

    if (latest.end <= res.availble_time) {
      res = {
        ...info,
        work_station_id: info.id,
        sub_station_id: item.station,
        availble: false,
        availble_time: latest.end,
        stations: undefined,
      };
    }
    // // compare
    // item.tasks = (orders).concat(waitings).filter((o) => (o.used_station || {}).work_station_id == info.id && (o.used_station || {}).sub_station_id == item.station);
    // let latest = _.last(item.tasks) || {};
    // if ((res.tasks || []).length > 0) {
    //   let myLatest = _.last(item.tasks);
    //   // console.log('myLatest.end', myLatest.end);
    //   // console.log('latest.end', latest.end);
    //   if (myLatest.end < latest.end) {
    //     res = {
    //       ...info,
    //       sub_station_id: res.station,
    //       availble: false,
    //       availble_time: latest.end,
    //       stations: undefined,
    //     };
    //   }
    // } else {
    //   res = {
    //     ...info,
    //     sub_station_id: res.station,
    //     availble: false,
    //     availble_time: latest.end,
    //     stations: undefined,
    //   };
    // }
  }

  return res;
}

async function waitRemainExtra(item) {
  let extra = relationExtra.find((r) => r.main_id == item.menu_id);
  if (extra == undefined) {
    return false;
  }
  let remain = remainings.find((r) => r.id == extra.extra_id) || {};
  if (remain.qty > 0) {
    return false;
  }

  let inprogress = orders.concat(waitings).filter((re) => re.menu_id == extra.extra_id) || [];
  // console.log('inprogress', JSON.stringify(inprogress));
  // console.log('waitRemainExtra---->');
  let acc = 0;
  for (var i = 0; i < inprogress.length; i++) {
    let inp = inprogress[i];
    // console.log('inp', inp.end);
    let used = orders.concat(waitings).filter((re) => re.is_extra != 1 && re.menu_id == item.menu_id) || [];
    let usedQty = used.reduce((total, item) => {
      return total + item.total_qty || 0;
    }, 0);
    acc += inp.total_qty;

    // console.log('inp.total_qty', acc, usedQty, inp.end, acc >= usedQty);

    if (acc > usedQty) {
      return {
        waiting: true,
        available_time: inp.end,
      };
    }
    // console.log('inp', inp.total_qty, usedQty);
  }
  return false;
}

async function chef(inputs) {
  const count = 3;
  let chefs = [];
  for (var c = 0; c < count; c++) {
    chefs.push({chef: c + 1, orders: []});
  }

  let stationList = await getStationList();

  // chef assign
  let waitings = [];
  // let items = inputs.concat(waiting);
  let items = inputs;
  // for (let i = 0; i < items.length; i++) {
  // let i = items.length;
  var i = 0;
  while (items.length > 0) {
    console.log('---------> start');
    let item = await getInfo(items[i]);
    let info = menu.find((im) => im.id == item.menu_id);
    // let station = workStation.find((ws) => ws.id == item.work_station_id);
    // console.log(info.cook_time);
    console.log('item', info.name);

    let nextChef = await getNextChef(chefs);
    for (let j = 0; j < chefs.length; j++) {
      if (nextChef.chef == chefs[j].chef) {
        let cookStart = await chefCookStart(nextChef);
        // let all = _.orderBy(_.flatten(chefs.map((ta) => ta.orders)), ['start'], ['desc']);
        let all = _.flatten(chefs.map((ta) => ta.orders));

        let found = false;
        for (let w = 0; w < waitings.length; w++) {
          cookStart = await chefCookStart(nextChef);
          let waiting = await getInfo(waitings[w]);
          let timeW = await getCookTime(waiting);
          let avaw = await checkStationAvailable(waiting, all, cookStart);
          console.log('avaw waiting ------>', waiting.info.name, avaw);
          if (avaw == true) {
            chefs[j].orders.push({
              ...waiting,
              chef: j + 1,
              cook_time: timeW.cookTime,
              start: cookStart.format(format),
              end: cookStart.add(timeW.minutes, 'minutes').format(format),
              is_extra: waiting.info.is_extra,
            });
            waitings.splice(w, 1);
            ++w;
            waitings.push(item);
            ++w;
            found = true;
            continue;
          }
        }
        // console.log('waitings 2', JSON.stringify(waitings));
        if (found) continue;

        // normal
        let ava = await checkStationAvailable(item, all, cookStart);
        console.log('------->ava', ava);
        let time = await getCookTime(item);
        if (ava == true) {
          chefs[j].orders.push({
            ...item,
            chef: j + 1,
            cook_time: time.cookTime,
            start: cookStart.format(format),
            end: cookStart.add(time.minutes, 'minutes').format(format),
            is_extra: info.is_extra,
          });
        } else {
          waitings.push(item);
        }
      }
    }

    items.shift();
  }

  for (let q = 0; q < waitings.length; q++) {
    let nextChef = await getNextChef(chefs);
    let waiting = await getInfo(waitings[q]);
    let timeW = await getCookTime(waiting);
    console.log('nextChef', nextChef.chef, waiting.info.name);
    for (let j = 0; j < chefs.length; j++) {
      if (nextChef.chef == chefs[j].chef) {
        // check
        let all = _.flatten(chefs.map((ta) => ta.orders));
        let cookStart = await getLastStationAvailable(waiting, all);
        chefs[j].orders.push({
          ...waiting,
          chef: j + 1,
          cook_time: timeW.cookTime,
          start: cookStart.format(format),
          end: cookStart.add(timeW.minutes, 'minutes').format(format),
          is_extra: waiting.info.is_extra,
        });
      }
    }
  }

  // waitings.map((atm) => {
  //   let child = (atm.child || []).reduce((total, b) => {
  //     return total + b.qty;
  //   }, 0);
  //   console.log(`${atm.queue}, ${atm.start}, ${atm.end}, ${atm.meunu_name}, ${atm.qty + child} qty, `);
  //   // atm.qty = atm.child.length + 1;
  //   // return atm
  // });

  // console.log('waitings', JSON.stringify(waitings));
  return chefs;
}

async function getInfo(item) {
  let info = await menu.find((im) => im.id == item.menu_id);
  let station = await workStation.find((ws) => ws.id == (info || {}).work_station_id);

  return {
    ...item,
    info,
    station,
  };
}

async function getCookTime(item) {
  // let info = await getInfo(item).info;
  let cookTime;
  let minutes;
  if (item.cook_time != undefined) {
    cookTime = item.cook_time;
    minutes = item.cook_time;
  } else {
    if (item.info != undefined) {
      cookTime = item.info.cook_time;
      minutes = item.info.cook_time;
      if (item.child.length > 0) {
        minutes = item.info.cook_time + item.info.plus_time * item.child.length;
      }
    }
  }

  return {
    cookTime: cookTime,
    minutes: minutes,
  };
}

async function getLastStationAvailable(item, orders) {
  console.log('------------------- start get last ---------------------------', item.info.name);
  console.log('item', item);

  let stations = orders.filter((atem) => {
    return atem.work_station_id == item.info.work_station_id;
  });
  stations = _.reverse(stations);

  // console.log('stations', JSON.stringify(stations));

  // // let stations = _.orderBy(orders, ['start', ['desc']]).filter((atem) => {
  // //   return (atem.work_station_id == item.work_station_id);
  // // });

  // if (stations.length == 0) {
  //   return true;
  // }

  // if (stations.length < station.qty) {
  //   return true;
  // }

  if (stations.length % item.station.qty == 0) {
    let items = stations.slice(0, item.station.qty);
    items = _.orderBy(items, ['end'], ['asc']);
    return datetime.moment(`${day} ${items[0].end}`).add(1, 'minutes');
    // for (var i = 0; i < items.length; i++) {
    //   // console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
    //   // console.log('s00> ', JSON.stringify(stations))
    //   // if (timetoStart.format("HH:mm:ss") > items[i].end) {
    //   //   return true;
    //   // }
    // }
  } else {
    // console.log("aaaaaa--->", )
    let items = stations.slice(0, stations.length % item.station.qty);
    items = _.orderBy(items, ['end'], ['asc']);
    return datetime.moment(`${day} ${items[0].end}`).add(1, 'minutes');
  }
  // else {
  //   // let items = stations.slice(0, stations.length % station.qty);
  //   // for (var i = 0; i < items.length; i++) {
  //   //   console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
  //   //   if (timetoStart.format("HH:mm:ss") > items[i].end) {
  //   //     return true;
  //   //   }
  //   // }
  // }
  // return false;
}

async function checkStationAvailable(item, orders, timetoStart) {
  console.log('------------------- start check ---------------------------', item.info.name, timetoStart.format('HH:mm:ss'));
  let info = menu.find((im) => im.id == item.menu_id);
  let station = workStation.find((ws) => ws.id == item.work_station_id);
  if (info.work_station_id == 0) {
    return true;
  }

  let stations = orders.filter((atem) => {
    return atem.work_station_id == item.work_station_id;
  });
  stations = _.reverse(stations);
  // let stations = _.orderBy(orders, ['start', ['desc']]).filter((atem) => {
  //   return (atem.work_station_id == item.work_station_id);
  // });

  if (stations.length == 0) {
    return true;
  }

  if (stations.length < station.qty) {
    return true;
  }

  if (stations.length % station.qty == 0) {
    let items = stations.slice(0, station.qty);
    for (var i = 0; i < items.length; i++) {
      // console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
      // console.log('s00> ', JSON.stringify(stations))
      if (timetoStart.format('HH:mm:ss') > items[i].end) {
        return true;
      }
    }
  }
  // else {
  //   // let items = stations.slice(0, stations.length % station.qty);
  //   // for (var i = 0; i < items.length; i++) {
  //   //   console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
  //   //   if (timetoStart.format("HH:mm:ss") > items[i].end) {
  //   //     return true;
  //   //   }
  //   // }
  // }
  return false;
}

function chefCookStart(inputChef) {
  let lastOrder = _.last(inputChef.orders);
  let cookStart = datetime.moment(`${day} ${timenow}`);
  if (lastOrder != undefined) {
    console.log('lastOrder', lastOrder.end);
    cookStart = datetime.moment(`${day} ${lastOrder.end}`).add(1, 'minutes');
  }

  console.log('cookStart', cookStart.format('HH:mm:ss'));
  return cookStart;
}

async function getChefList() {
  const count = 3;
  let chefs = [];
  for (var c = 0; c < count; c++) {
    chefs.push({chef: c + 1, orders: []});
  }

  return chefs;
}

async function getStationList() {
  let res = [];

  for (var s = 0; s < workStation.length; s++) {
    let station = workStation[s];
    // console.log('var', workStation[s].id);
    const count = station.qty;
    let stations = [];
    for (var c = 0; c < count; c++) {
      stations.push({station: c + 1, tasks: []});
    }
    res.push({
      ...station,
      stations,
    });
  }
  // console.log('res', JSON.stringify(res));
  return res;
}

async function queuingWaitings() {
  for (var i = 0; i < waitings.length; i++) {
    let nextChef = getNextChefWithWaitings();
    waitings[i].chef_id = nextChef.chef;
  }
}
// function chefxExtra(items) {
//   const count = 3;
//   let chefs = [];

//   for (var i = 0; i < count; i++) {
//     chefs.push({chef: i + 1, orders: []});
//   }

//   // chef assign
//   for (let i = 0; i < items.length; i++) {
//     let item = items[i];
//     let info = menu.find((im) => im.id == item.menu_id);
//     // console.log(info.cook_time);

//     // let cookTime;
//     // let minutes;
//     // if (item.cook_time != undefined) {
//     //   cookTime = item.cook_time;
//     //   minutes = item.cook_time;
//     // } else {
//     //   cookTime = info.cook_time;
//     //   minutes = info.cook_time;
//     //   if (item.child.length > 0) {
//     //     minutes = info.cook_time + info.plus_time * item.child.length;
//     //   }
//     // }

//     let nextChef = getNextChef(chefs);

//     for (let j = 0; j < chefs.length; j++) {
//       if (nextChef.chef == chefs[j].chef) {
//         let lastOrder = _.last(nextChef.orders);

//         let cookStart = datetime.moment(`${day} ${timenow}`);
//         if (lastOrder != undefined) {
//           cookStart = datetime.moment(`${day} ${lastOrder.end}`).add(1, 'minutes');
//         }

//         // console.log(cookStart.format(format));
//         let task = {
//           ...item,
//           chef: j + 1,
//           cook_time: cookTime,
//           start: cookStart.format(format),
//           end: cookStart.add(minutes, 'minutes').format(format),
//         };
//         if ((info || {}).is_extra != 1) {
//           chefs[j].orders.push(task);
//         }
//       }
//     }
//   }

//   let extraMenus = items.filter((item) => item.is_extra === 1);
//   for (var t = 0; t < extraMenus.length; t++) {
//     for (let c = 0; c < chefs.length; c++) {}

//     console.log('t', extraMenus[t].meunu_name);
//   }

//   console.log('chefs', JSON.stringify(chefs));

//   return chefs;
// }

function getNextChef() {
  for (let i = 0; i < chefs.length; i++) {
    chefs[i].orders = orders.filter((o) => o.chef_id == i + 1);
  }

  for (let a = 0; a < chefs.length; a++) {
    if (chefs[a].orders == 0) {
      return {
        ...chefs[a],
        available_time: null,
      };
    }
  }

  let res = {};
  for (let b = 0; b < chefs.length; b++) {
    let myLatest = _.last(chefs[b].orders);
    if ((res.orders || []).length > 0) {
      let latest = _.last(res.orders);
      if (myLatest.end < latest.end) {
        res = {
          ...chefs[b],
          available_time: myLatest.end,
        };
      }
    } else {
      res = {
        ...chefs[b],
        available_time: myLatest.end,
      };
    }
  }
  return res;
}

function getNextChefWithWaitings() {
  let makings = orders.map((o) => {
    return o.make_with || [];
  });

  for (let i = 0; i < chefs.length; i++) {
    chefs[i].orders = orders
      .concat(waitings)
      .concat(makings)
      .filter((o) => o.chef_id == i + 1);
  }

  for (let a = 0; a < chefs.length; a++) {
    if (chefs[a].orders == 0) {
      return {
        ...chefs[a],
        available_time: null,
      };
    }
  }

  let res = {};
  for (let b = 0; b < chefs.length; b++) {
    let myLatest = _.last(chefs[b].orders);
    if ((res.orders || []).length > 0) {
      let latest = _.last(res.orders);
      if (myLatest.end < latest.end) {
        res = {
          ...chefs[b],
          available_time: myLatest.end,
        };
      }
    } else {
      res = {
        ...chefs[b],
        available_time: myLatest.end,
      };
    }
  }
  return res;
}

async function getOrderList() {
  let temp = _.last(items);
  let list = JSON.parse(orderList || `[]`) || [];
  list = list.filter((li) => li.is_extra == 1 || li.start > temp.time);
  orders = list;
}

async function getWaitingList() {
  let temp = _.last(items);
  let list = JSON.parse(waitingList || `[]`) || [];
  list = list.filter((li) => li.start > temp.time);
  waitings = list;
}

// function addExtraTask(items) {
//   let res = _.cloneDeep(items);
//   for (let i = 0; i < res.length; i++) {
//     // console.log('res', res);
//     let item = res[i];
//     let info = menu.find((im) => im.id == item.menu_id);
//     if ((info || {}).is_extra === 1 && i > 0) {
//       // console.log('i', i, item.meunu_name);
//       let j = i - 1;
//       let sum = 0;
//       let insertAt = i;
//       while (j >= 0) {
//         let before = res[j];
//         sum += before.cook_time;
//         console.log('--->', before.meunu_name, before.cook_time, sum);
//         if (sum >= item.cook_time) {
//           insertAt = j + 1;
//           break;
//         }
//         j--;
//       }

//       // item.cook_time = item.cook_time - (sum - res[j+1].cook_time);
//       // console.log('sum', sum);
//       const task = {
//         ...item,
//         menu_id: undefined,
//         is_extra: undefined,
//         meunu_name: `extra ${info.name}`,
//         cook_time: info.prepare_time,
//         main_id: info.menu_id,
//       };
//       res.splice(insertAt, 0, task);
//       ++i;
//       // console.log('insertAt', insertAt);
//     }
//   }

//   res = res.map((item, idx) => {
//     item.queue = idx + 1;
//     delete item.start;
//     delete item.end;
//     delete item.chef;
//     return item;
//   });
//   // console.log('res', JSON.stringify(res));

//   res = chefxExtra(res);
//   res = _.orderBy(_.flatten(res.map((ta) => ta.orders)), ['start'], ['asc']);

//   res.map((atm) => {
//     console.log(`${atm.queue}, ${atm.start}, ${atm.end}, ${atm.qty}, ${atm.cook_time}, ${atm.meunu_name}, `);
//     // atm.qty = atm.child.length + 1;
//     // return atm
//   });

//   // console.log('res', JSON.stringify(res));
//   return res;
// }

calculate();
