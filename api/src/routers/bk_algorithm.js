import express from 'express';
import redis from 'redis';
import _, {at, ceil} from 'lodash';

import {resp} from '../utils/resp';
import {controller} from '../controllers/controllers';
import '../data/menu';
import {menu, workStation} from '../data/menu';
import {items} from '../data/orders';
import {datetime} from '../utils/time';

let day = '2020-10-17';
let timenow = `12:00:00`;
let format = 'HH:mm:ss';

async function calculate() {
  const groups = await grouping(items);
  const chefs = await chef(groups);
  const temp = _.orderBy(_.flatten(chefs.map((ta) => ta.orders)), ['start'], ['asc']);
  // const added = addExtraTask(temp);
  // console.log(JSON.stringify(temp));

  // let result = added.map(atm => {
  //   atm.qty = atm.child.length + 1;
  //   return atm
  // })

  temp.map((atm) => {
    let child = (atm.child || []).reduce((total, b) => {
      return total + b.qty;
    }, 0);

    console.log(`${atm.queue}, ${atm.chef}, ${atm.start}, ${atm.end}, ${atm.meunu_name}, ${atm.qty + child} qty, `);
    // atm.qty = atm.child.length + 1;
    // return atm
  });

  // console.log(JSON.stringify(result));
}

function grouping(items) {
  // split - items
  let splited = [];
  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    let info = menu.find((m) => m.id == item.menu_id) || {};
    let station = workStation.find((s) => s.id == info.work_station_id) || {};

    for (let sp = 0; sp < item.qty; sp++) {
      splited.push({
        ...item,
        qty: 1,
        work_station_id: station.id,
      });
    }

    // if (item.qty > station.max_capacity) {
    //   //
    //   // let round = ceil(item.qty / station.max_capacity);
    //   // let remain = item.qty;
    //   // for (let s = 0; s < round; s++) {
    //   //   remain -= station.max_capacity;
    //   //   let newQty = remain > 0 ? station.max_capacity : remain + station.max_capacity;
    //   //   splited.push({
    //   //     ...item,
    //   //     qty: newQty,
    //   //   });
    //   // }
    // } else {
    //   splited.push(item);
    // }
  }

  // console.log('splited', JSON.stringify(splited));

  let res = [];
  let queue = 1;
  for (let o = 0; o < splited.length; o++) {
    let item = splited[o];
    let info = menu.find((m) => m.id == item.menu_id) || {};
    let station = workStation.find((s) => s.id == info.work_station_id);

    let nexts = splited.slice(o + 1);

    if (item.queue != undefined || item.menu_id == undefined) {
      continue;
    }

    let temp = {
      ...item,
      queue,
      child: [],
    };

    for (let j = 0; j < nexts.length; j++) {
      if (nexts[j].menu_id == item.menu_id) {
        // // add group menu
        // orders.splice(i+1, 0, nexts[j])
        // check capacity
        // console.log('item', item.meunu_name, item.eo);
        // (item.child || []).length
        let childQty = 0;
        if ((temp.child || []).length > 0) {
          childQty = temp.child.reduce(function (total, b) {
            return total + b.qty;
          }, 0);
        }

        // console.log('childQty', childQty);
        let currectQty = item.qty + childQty;
        if (currectQty < info.max) {
          temp.child.push(nexts[j]);

          // remove group menu
          let key = j + o + 1;
          splited.splice(key, 1, {});
        }
      }
    }
    res.push(temp);
    queue++;
  }

  console.log('res', JSON.stringify(res));
  return res;
}

async function chef(inputs) {
  const count = 3;
  let chefs = [];
  for (var c = 0; c < count; c++) {
    chefs.push({chef: c + 1, orders: []});
  }

  let stationList = await getStationList();

  // chef assign
  let waitings = [];
  // let items = inputs.concat(waiting);
  let items = inputs;
  // for (let i = 0; i < items.length; i++) {
  // let i = items.length;
  var i = 0;
  while (items.length > 0) {
console.log('---------> start' );
    let item = await getInfo(items[i]);
    let info = menu.find((im) => im.id == item.menu_id);
    // let station = workStation.find((ws) => ws.id == item.work_station_id);
    // console.log(info.cook_time);
    console.log('item', info.name);

    let nextChef = await getNextChef(chefs);
    for (let j = 0; j < chefs.length; j++) {
      if (nextChef.chef == chefs[j].chef) {
        let cookStart = await chefCookStart(nextChef);
        // let all = _.orderBy(_.flatten(chefs.map((ta) => ta.orders)), ['start'], ['desc']);
        let all = _.flatten(chefs.map((ta) => ta.orders));

        let found = false;
        for (let w = 0; w < waitings.length; w++) {
          cookStart = await chefCookStart(nextChef);
          let waiting = await getInfo(waitings[w]);
          let timeW = await getCookTime(waiting);
          let avaw = await checkStationAvailable(waiting, all, cookStart);
          console.log('avaw waiting ------>', waiting.info.name, avaw);
          if (avaw == true) {
            chefs[j].orders.push({
              ...waiting,
              chef: j + 1,
              cook_time: timeW.cookTime,
              start: cookStart.format(format),
              end: cookStart.add(timeW.minutes, 'minutes').format(format),
              is_extra: waiting.info.is_extra,
            });
            waitings.splice(w, 1);
            ++w;
            waitings.push(item);
            ++w;
            found = true;
            continue;
          }
        }
        // console.log('waitings 2', JSON.stringify(waitings));
        if (found)
          continue;

        // normal
        let ava = await checkStationAvailable(item, all, cookStart);
        console.log('------->ava', ava);
        let time = await getCookTime(item);
        if (ava == true) {
          chefs[j].orders.push({
            ...item,
            chef: j + 1,
            cook_time: time.cookTime,
            start: cookStart.format(format),
            end: cookStart.add(time.minutes, 'minutes').format(format),
            is_extra: info.is_extra,
          });
        } else {
          waitings.push(item);
        }
      }
    }

    items.shift();
  }

  for (let q = 0; q < waitings.length; q++) {
    let nextChef = await getNextChef(chefs);
    let waiting = await getInfo(waitings[q]);
    let timeW = await getCookTime(waiting);
    console.log('nextChef', nextChef.chef, waiting.info.name);
    for (let j = 0; j < chefs.length; j++) {
      if (nextChef.chef == chefs[j].chef) {
        // check 
        let all = _.flatten(chefs.map((ta) => ta.orders));
        let cookStart = await getLastStationAvailable(waiting, all);
        chefs[j].orders.push({
          ...waiting,
          chef: j + 1,
          cook_time: timeW.cookTime,
          start: cookStart.format(format),
          end: cookStart.add(timeW.minutes, 'minutes').format(format),
          is_extra: waiting.info.is_extra,
        });
      }
    }
  }

  // waitings.map((atm) => {
  //   let child = (atm.child || []).reduce((total, b) => {
  //     return total + b.qty;
  //   }, 0);
  //   console.log(`${atm.queue}, ${atm.start}, ${atm.end}, ${atm.meunu_name}, ${atm.qty + child} qty, `);
  //   // atm.qty = atm.child.length + 1;
  //   // return atm
  // });

  // console.log('waitings', JSON.stringify(waitings));
  return chefs;
}

async function getInfo(item) {
  let info = await menu.find((im) => im.id == item.menu_id);
  let station = await workStation.find((ws) => ws.id == item.work_station_id);
  
  return {
    ...item,
    info,
    station,
  }
}

async function getCookTime(item) {
  // let info = await getInfo(item).info;
  let cookTime;
  let minutes;
  if (item.cook_time != undefined) {
    cookTime = item.cook_time;
    minutes = item.cook_time;
  } else {
    if (item.info != undefined) {
      cookTime = item.info.cook_time;
      minutes = item.info.cook_time;
      if (item.child.length > 0) {
        minutes = item.info.cook_time + item.info.plus_time * item.child.length;
      }
    }
  }

  return {
    cookTime: cookTime,
    minutes: minutes,
  }
}

async function getLastStationAvailable(item, orders) {
  console.log("------------------- start get last ---------------------------", item.info.name)
  console.log('item', item);


  let stations = orders.filter((atem) => {
    return (atem.work_station_id == item.info.work_station_id);
  });
  stations = _.reverse(stations);

// console.log('stations', JSON.stringify(stations));

  // // let stations = _.orderBy(orders, ['start', ['desc']]).filter((atem) => {
  // //   return (atem.work_station_id == item.work_station_id);
  // // });

  // if (stations.length == 0) {
  //   return true;
  // }

  // if (stations.length < station.qty) {
  //   return true;
  // }

  if (stations.length % item.station.qty == 0) {
    let items = stations.slice(0, item.station.qty);
    items = _.orderBy(items, ['end'], ['asc']);
    return datetime.moment(`${day} ${items[0].end}`).add(1, 'minutes');
    // for (var i = 0; i < items.length; i++) {
    //   // console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
    //   // console.log('s00> ', JSON.stringify(stations))
    //   // if (timetoStart.format("HH:mm:ss") > items[i].end) {
    //   //   return true;
    //   // }
    // }
  } else {
    // console.log("aaaaaa--->", )
    let items = stations.slice(0, stations.length % item.station.qty);
    items = _.orderBy(items, ['end'], ['asc']);
    return datetime.moment(`${day} ${items[0].end}`).add(1, 'minutes');
  }
  // else {
  //   // let items = stations.slice(0, stations.length % station.qty);
  //   // for (var i = 0; i < items.length; i++) {
  //   //   console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
  //   //   if (timetoStart.format("HH:mm:ss") > items[i].end) {
  //   //     return true;
  //   //   }
  //   // }
  // }
  // return false;
}

async function checkStationAvailable(item, orders, timetoStart) {
  console.log("------------------- start check ---------------------------", item.info.name, timetoStart.format("HH:mm:ss"))
  let info = menu.find((im) => im.id == item.menu_id);
  let station = workStation.find((ws) => ws.id == item.work_station_id);
  if (info.work_station_id == 0) {
    return true;
  }

  let stations = orders.filter((atem) => {
    return (atem.work_station_id == item.work_station_id);
  });
  stations = _.reverse(stations);
  // let stations = _.orderBy(orders, ['start', ['desc']]).filter((atem) => {
  //   return (atem.work_station_id == item.work_station_id);
  // });

  if (stations.length == 0) {
    return true;
  }

  if (stations.length < station.qty) {
    return true;
  }

  if (stations.length % station.qty == 0) {
    let items = stations.slice(0, station.qty);
    for (var i = 0; i < items.length; i++) {
      // console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
      // console.log('s00> ', JSON.stringify(stations))
      if (timetoStart.format("HH:mm:ss") > items[i].end) {
        return true;
      }
    }
  }
  // else {
  //   // let items = stations.slice(0, stations.length % station.qty);
  //   // for (var i = 0; i < items.length; i++) {
  //   //   console.log('items ', stations.length , " | ", timetoStart.format("HH:mm:ss"), items[i].end, stations.length, station.qty, stations.length % station.qty);
  //   //   if (timetoStart.format("HH:mm:ss") > items[i].end) {
  //   //     return true;
  //   //   }
  //   // }
  // }
  return false;
}

function chefCookStart(inputChef) {
  let lastOrder = _.last(inputChef.orders);
  let cookStart = datetime.moment(`${day} ${timenow}`);
  if (lastOrder != undefined) {
console.log('lastOrder', lastOrder.end);
    cookStart = datetime.moment(`${day} ${lastOrder.end}`).add(1, 'minutes');
  }

  
  console.log('cookStart', cookStart.format("HH:mm:ss"));
  return cookStart;
}

async function getStationList() {
  let res = [];
  
  for (var s = 0; s < workStation.length; s++) {
    let station = workStation[s];
    // console.log('var', workStation[s].id);
    const count = station.qty;
    let stations = [];
    for (var c = 0; c < count; c++) {
      stations.push({station: c + 1, taks: []});
    }
    res.push({
      ...station,
      stations
    })
  }

  console.log('res', JSON.stringify(res));
  return res;
}

// function chefxExtra(items) {
//   const count = 3;
//   let chefs = [];

//   for (var i = 0; i < count; i++) {
//     chefs.push({chef: i + 1, orders: []});
//   }

//   // chef assign
//   for (let i = 0; i < items.length; i++) {
//     let item = items[i];
//     let info = menu.find((im) => im.id == item.menu_id);
//     // console.log(info.cook_time);

//     // let cookTime;
//     // let minutes;
//     // if (item.cook_time != undefined) {
//     //   cookTime = item.cook_time;
//     //   minutes = item.cook_time;
//     // } else {
//     //   cookTime = info.cook_time;
//     //   minutes = info.cook_time;
//     //   if (item.child.length > 0) {
//     //     minutes = info.cook_time + info.plus_time * item.child.length;
//     //   }
//     // }

//     let nextChef = getNextChef(chefs);

//     for (let j = 0; j < chefs.length; j++) {
//       if (nextChef.chef == chefs[j].chef) {
//         let lastOrder = _.last(nextChef.orders);

//         let cookStart = datetime.moment(`${day} ${timenow}`);
//         if (lastOrder != undefined) {
//           cookStart = datetime.moment(`${day} ${lastOrder.end}`).add(1, 'minutes');
//         }

//         // console.log(cookStart.format(format));
//         let task = {
//           ...item,
//           chef: j + 1,
//           cook_time: cookTime,
//           start: cookStart.format(format),
//           end: cookStart.add(minutes, 'minutes').format(format),
//         };
//         if ((info || {}).is_extra != 1) {
//           chefs[j].orders.push(task);
//         }
//       }
//     }
//   }

//   let extraMenus = items.filter((item) => item.is_extra === 1);
//   for (var t = 0; t < extraMenus.length; t++) {
//     for (let c = 0; c < chefs.length; c++) {}

//     console.log('t', extraMenus[t].meunu_name);
//   }

//   console.log('chefs', JSON.stringify(chefs));

//   return chefs;
// }

function getNextChef(chefs) {
  for (let i = 0; i < chefs.length; i++) {
    if (chefs[i].orders.length == 0) {
      return chefs[i];
    }
  }

  let res = {};
  for (let b = 0; b < chefs.length; b++) {
    if ((res.orders || []).length > 0) {
      let latest = _.last(res.orders);
      let myLatest = _.last(chefs[b].orders);
      if (myLatest.end < latest.end) {
        res = chefs[b];
      }
    } else {
      res = chefs[b];
    }
  }
  return res;
}

function addExtraTask(items) {
  let res = _.cloneDeep(items);
  for (let i = 0; i < res.length; i++) {
    // console.log('res', res);
    let item = res[i];
    let info = menu.find((im) => im.id == item.menu_id);
    if ((info || {}).is_extra === 1 && i > 0) {
      // console.log('i', i, item.meunu_name);
      let j = i - 1;
      let sum = 0;
      let insertAt = i;
      while (j >= 0) {
        let before = res[j];
        sum += before.cook_time;
        console.log('--->', before.meunu_name, before.cook_time, sum);
        if (sum >= item.cook_time) {
          insertAt = j + 1;
          break;
        }
        j--;
      }

      // item.cook_time = item.cook_time - (sum - res[j+1].cook_time);
      // console.log('sum', sum);
      const task = {
        ...item,
        menu_id: undefined,
        is_extra: undefined,
        meunu_name: `extra ${info.name}`,
        cook_time: info.prepare_time,
        main_id: info.menu_id,
      };
      res.splice(insertAt, 0, task);
      ++i;
      // console.log('insertAt', insertAt);
    }
  }

  res = res.map((item, idx) => {
    item.queue = idx + 1;
    delete item.start;
    delete item.end;
    delete item.chef;
    return item;
  });
  // console.log('res', JSON.stringify(res));

  res = chefxExtra(res);
  res = _.orderBy(_.flatten(res.map((ta) => ta.orders)), ['start'], ['asc']);

  res.map((atm) => {
    console.log(`${atm.queue}, ${atm.start}, ${atm.end}, ${atm.qty}, ${atm.cook_time}, ${atm.meunu_name}, `);
    // atm.qty = atm.child.length + 1;
    // return atm
  });

  // console.log('res', JSON.stringify(res));
  return res;
}

calculate();
