import express from 'express';
import { resp } from '../utils/resp'
import { controller } from '../controllers/controllers'

export const router = express.Router();
router.post('/webhook', webhook);

export async function webhook(req, res, next) {
  try {
    const start = process.hrtime()
    await controller.line.webhook(req)
    res.on('finish', controller.line.checkResponseTime(start, req))
    next(resp({ data: '' }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}
