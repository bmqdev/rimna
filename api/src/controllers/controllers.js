import { FindingController } from './FindingController';
import { LineController } from './LineController';
import { GoogleController } from './GoogleController';

export const controller = {
  finding: new FindingController(),
  line: new LineController(),
  google: new GoogleController(),
};
