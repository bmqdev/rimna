import request from 'request-promise';
import config from '../config/config'

export class GoogleController {
  async bestway(origin, destination) {
    try {
      const res = await request.get(`https://maps.googleapis.com/maps/api/directions/json?origin=${encodeURI(origin)}&destination=${encodeURI(destination)}&key=${config.google.key}&mode=driving`)
      .then((res) => {
        return res
      }).catch((err) => {
        return err
      });
      return JSON.parse(res);
    } catch (e) {
      console.log(e);
    }
  }
}