import { get } from 'lodash'
import config from '../config/config.js';
import request from 'request-promise';
import { getDurationInMilliseconds } from '../utils/time.js';

const sdk = require('@line/bot-sdk');
const client = new sdk.Client(config.line);

export class LineController {
  async webhook(req) {
    try {
      console.log('req-->', req.body)
      const events = get(req, 'body.events');
      if (!events) {
        throw new Error('Invalid events');
      }

      for (let event of events) {
        await this.handleEvent(event);
      }
    } catch (e) {
      this.notifyError(e.message)
      throw e
    }
  }

  async handleEvent(event) {
    switch (event.type) {
      case 'message':
        const message = event.message;
        // test slow answer 
        if (message.text === 'slow' || message.text === 'Slow') {
          await new Promise(resolve => setTimeout(resolve, 11 * 1000))
          return;
        }
        switch (message.type) {
          case 'text':
            return this.handleText(message, event.replyToken);
          case 'image':
            return this.handleImage(message, event.replyToken);
          case 'video':
            return this.handleVideo(message, event.replyToken);
          case 'audio':
            return this.handleAudio(message, event.replyToken);
          case 'location':
            return this.handleLocation(message, event.replyToken);
          case 'sticker':
            return this.handleSticker(message, event.replyToken);
          default:
            throw new Error(`Unknown message: ${JSON.stringify(message)}`);
        }

      default:
        throw new Error(`Unknown event: ${JSON.stringify(event)}`);
    }
  }

  handleText(message, replyToken) {
    return this.replyText(replyToken, message.text);
  }

  handleImage(message, replyToken) {
    return this.replyText(replyToken, 'Got Image');
  }

  handleVideo(message, replyToken) {
    return this.replyText(replyToken, 'Got Video');
  }

  handleAudio(message, replyToken) {
    return this.replyText(replyToken, 'Got Audio');
  }

  handleLocation(message, replyToken) {
    return this.replyText(replyToken, 'Got Location');
  }

  handleSticker(message, replyToken) {
    return this.replyText(replyToken, 'Got Sticker');
  }

  replyText(token, texts) {
    texts = Array.isArray(texts) ? texts : [texts];
    return client.replyMessage(
      token,
      texts.map((text) => {
        return { type: 'text', text: text };
      })
    );
  };

  async notifyError(message) {
    try {
      request({
        method: 'POST',
        uri: 'https://notify-api.line.me/api/notify',
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        auth: {
          bearer: 'tKtuHxsHkJbfDBS7hWqTwHkeKukiO6Eaw7lsMvl5zlS', //token
        },
        form: {
          message,
        },
      }).then((res) => {
        console.log('ten', res)
      }).catch((err) => {
        console.log('catch', err)
      });
    } catch (e) {
      console.log(e);
    }
  }

  async checkResponseTime(start, req) {
    const duration = getDurationInMilliseconds(start)
    console.log(`[FINISHED] ${duration.toLocaleString()} ms`)
    if (duration > 10 * 1000) {
      this.notifyError(`Bot responding too slow: ${JSON.stringify(req.body)}`)
    }
  }
}